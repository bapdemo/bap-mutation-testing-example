package mutation.testing;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleTest {
    @Test
    public void testMethod() {
        var simple = new Simple();
        assertEquals(33L, simple.test(33L));
    }
}