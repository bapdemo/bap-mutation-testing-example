
# mutation testing example

Analyze the `class Rational` (and fix the test cases to _kill all mutants_ generted by <http://pitest.org>)

## mvn goals

```
mvn clean
mvn test-compile org.pitest:pitest-maven:mutationCoverage 
```

## reports

Are to be found in [target/pit-reports](./target/pit-reports)